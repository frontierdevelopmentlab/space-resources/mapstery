"""
Test file for `map.py` functions
"""
import os
import sys
import numpy as np
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import mapstery


def test_save_imagery():
    """ Test saving map image as geotiff """
    carto = mapstery.Map()
    m_mat = np.ones((35, 50)) * 128.0
    carto.add_band(m_mat, "NEW")
    carto.save("/tmp/mastery_test0.gtif")


def test_bands():
    """ Test band values generated by mapstery.Map() """
    z_map = mapstery.Map()
    m_mat = np.ones((10, 15)) * 1.0
    m_mat[5][5] = 0.0
    m_mat[9][8] = 2.0
    z_map.add_band(m_mat, "NEW")

    a_mat = z_map.get_band(1).ReadAsArray()
    assert a_mat[9][8] == 2.0
    assert a_mat[8][9] == 1.0
    assert a_mat[5][5] == 0.0

def test_add_band():
    """ Test Map().add_band() method """
    z_map = mapstery.Map()
    m_mat = np.ones((10, 15)) * 1.0
    m_mat[5][5] = 0.0
    m_mat[9][8] = 2.0

    z_map.add_band(m_mat, "NEW")
    assert z_map.total_bands == 1

    z_map.add_band(m_mat, "NEW2")
    assert z_map.total_bands == 2
    z_map.add_band(m_mat, "NEW3")
    assert z_map.total_bands == 3
    z_map.add_band(m_mat, "NEW4")
    assert z_map.total_bands == 4


def test_get_band():
    """ Test Map().get_band() method """
    z_map = mapstery.Map()
    m_mat = np.ones((3, 3)) * 1.0
    m_mat[0][0] = 4.0
    m_mat[0][1] = 4.0
    m_mat[0][2] = 4.0
    z_map.add_band(m_mat, "BAND18")
    m_mat[0][0] = 1.0
    m_mat[0][1] = 1.0
    m_mat[0][2] = 1.0
    z_map.add_band(m_mat, "BAND9")

    b_map = z_map.get_band(1)
    assert b_map.GetMetadataItem("NAME") == "BAND18"
    assert np.mean(b_map.ReadAsArray()) == 2.0

    b_map = z_map.get_band_by_name("BAND9")
    assert b_map.GetMetadataItem("NAME") == "BAND9"
    assert np.mean(b_map.ReadAsArray()) == 1.0

    c_map = z_map.get_band("BAND9")
    assert c_map.GetMetadataItem("NAME") == "BAND9"
    assert np.mean(c_map.ReadAsArray()) == 1.0


def test_get_pixel():
    """ Test the Map().get_pixel() method """
    z_map = mapstery.Map()
    m_mat = np.ones((3, 3)) * 1.0
    m_mat[0][0] = 4.0
    m_mat[0][1] = 4.0
    m_mat[0][2] = 5.0
    z_map.add_band(m_mat, "BAND18")

    m_mat[0][0] = 1.0
    m_mat[0][1] = 2.0
    m_mat[0][2] = 3.0
    z_map.add_band(m_mat, "BAND9")

    z_map._dataset.FlushCache()

    p_array = z_map.get_pixel((1, 0))
    assert len(p_array) == 2
    assert p_array[0] == 4.0
    assert p_array[1] == 2.0

    p_array = z_map.get_pixel((2, 0), bands=[1])
    assert len(p_array) == 1
    assert p_array[0] == 5.0

    k_array = z_map.get_pixel((2, 0), bands=[1, "BAND9"])
    assert len(k_array) == 2
    assert k_array[0] == 5.0
    assert k_array[1] == 3.0

    k_array = z_map.get_pixel((2, 0), bands=["BAND9", 1])
    assert len(k_array) == 2
    assert k_array[0] == 3.0
    assert k_array[1] == 5.0


def test_transform_band():
    """ Test the Map().transform_band() method """
    z_map = mapstery.Map()
    m_mat = np.ones((3, 3)) * 1.0
    m_mat[0][0] = 4.0
    m_mat[0][1] = 4.0
    m_mat[0][2] = 5.0
    z_map.add_band(m_mat, "BAND18")

    m_mat[0][0] = 1.0
    m_mat[0][1] = 2.0
    m_mat[0][2] = 3.0
    z_map.add_band(m_mat, "BAND9")

    def func_square(a_mat):
        """ Test function for Map().transform_band() """
        return a_mat * a_mat

    c_mat = z_map.transform_band(1, func_square)

    assert c_mat[1][0] == 1.0
    assert c_mat[0][0] == 16.0
    assert c_mat[0][1] == 16.0
    assert c_mat[0][2] == 25.0
