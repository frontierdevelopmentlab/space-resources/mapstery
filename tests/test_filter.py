"""
Test file for `filter.py` functions
"""
import os
import sys
import numpy as np
sys.path.insert(
    0,
    os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import mapstery


def test_move_dynamics():
    """ Test function for filter.move_dynamics() """
    a_mat = np.array([0, 128, 255])
    b_mat = mapstery.filter.move_dynamics(
        a_mat, target_min_bound=0, target_max_bound=7, convert_uint=True)
    assert len(b_mat) == 3
    assert b_mat[0] == 0
    assert b_mat[1] == 3
    assert b_mat[2] == 7
    c_mat = mapstery.filter.move_dynamics(
        a_mat, target_min_bound=0, target_max_bound=7, convert_uint=False)
    assert len(c_mat) == 3
    assert c_mat[0] == 0.0
    assert c_mat[1] > 3.5
    assert c_mat[1] < 3.52
    assert c_mat[2] == 7.0

    # Non 0-255 arrays values have to use init_to_content=True
    d_mat = mapstery.filter.move_dynamics(
        np.array([1.0, 4.5, 8.0]), target_min_bound=-1.0, target_max_bound=1.0,
        init_to_content=True, convert_uint=False)
    assert len(d_mat) == 3
    assert d_mat[0] == -1.0
    assert d_mat[1] == 0.0
    e_mat = mapstery.filter.move_dynamics(
        np.array([0, 0.75, 2]), target_max_bound=1.0, init_to_content=True,
        convert_uint=False)
    assert len(e_mat) == 3
    assert e_mat[0] == 0.0
    assert e_mat[1] == 0.375
    assert e_mat[2] == 1.0


def test_filter_isogradient():
    """ Test function for filter.isogradient() """
    z_map = mapstery.Map()
    m_mat = np.ones((14, 15))*1.0
    m_mat[5][5] = 0.0
    m_mat[9][8] = 2.0
    z_map.add_band(m_mat, "NEW")

    a_mat = mapstery.filter.isogradient(z_map.get_band(1), blur_radius=0)
    assert a_mat.shape[0] == 14
    assert a_mat.shape[1] == 15
    assert a_mat[5][9] == 0.0
    assert a_mat[5][2] == 0.0
    assert a_mat[5][3] == 0.0
    assert a_mat[5][4] == 1.0 # 0.375 max mapping to 1.0
    assert a_mat[6][6] == 1.0
    assert a_mat[2][8] == 0.0

    assert m_mat[9][8] == 2.0
    assert a_mat[9][8] == 0.0
    assert a_mat[9][9] == 1.0 # 0.375 max mapping to 1.0


def test_polynomial_decrease():
    """ Test function for filter.polynomial_decrease() """
    a_mat = mapstery.filter.polynomial_decrease(4)
    assert len(a_mat) == 4
    assert a_mat[0] == 1.0
    assert a_mat[3] == 0.0


def test_integrate_shadows():
    """ Test function for filter.integrate_shadows() """
    carto = mapstery.Map()
    m_mat = np.ones((35, 50))*128.0
    carto.add_band(m_mat, "NEW")
    m_mat = np.ones((35, 50))*128.0
    carto.add_band(m_mat, "NEW")

    # Useless thing to do but testing the code runs
    mapstery.filter.integrate_shadows(
        carto, bands=[1, 2], integration_horizon=1)
