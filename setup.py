"""
Mapstery setuptools setup.

Created thanks to:
    https://packaging.python.org/en/latest/distributing.html
    https://github.com/pypa/sampleproject

"""
import codecs
from os import path
from setuptools import setup, find_packages

HERE = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with codecs.open(path.join(HERE, 'README.md'), encoding='utf-8') as f:
    LONG_DESCRIPTION = f.read()

setup(
    version="0.0.7",
    name="mapstery",
    description="Digital terrain map manipulation high level functions",
    long_description=LONG_DESCRIPTION,
    url="https://gitlab.com/frontierdevelopmentlab/space-resources/mapstery",
    license="BSD",
    author="Red Boumghar",
    install_requires=["gdal", "Pillow", "scipy", "numpy"],
    python_requires='>=3',
    extras_require={
        "test": ["pytest", "pytest-cov"]
    },
    packages=find_packages(exclude=["tests", "docs", "viewers", "examples"]),
    keywords="GDAL, python, raster, dataset, multilayer, multichannel, layers, channels, gis, information",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License"
    ],
)
