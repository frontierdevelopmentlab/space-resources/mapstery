#
# Helpful commands for packaging
#
# This makefile only concerns packaging and building.
# To be used only if you plan to build your own packages
# for different target platforms
#

LASTWHEELFILE=`ls -rt1 dist/ | tail -n1`

clean:
	rm -rf mapstery.egg-info/ build/
	@echo "Cleaning eventual __pycache__ directories"
	find mapstery/ -name "__pycache__"  -type d -exec rm -r "{}" \; > /dev/null 2>&1

build: clean
	@echo " ----- Building universal wheel package"
	python setup.py bdist_wheel
	@echo " ----- built mapstery: $(grep version setup.py)"

pypi: build
	twine upload --repository pypi dist/$(LASTWHEELFILE)

testpypi: build
	twine upload --repository testpypi dist/*
	@# twine upload --repository-url https://test.pypi.org/legacy/ dist/*

test:
	pytest -v tests

cov:
	pytest -v --cov=mapstery/ tests/
